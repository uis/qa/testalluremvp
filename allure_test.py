import allure

class TestAllure:

    def test_01(self):
        assert 1+1==2

    def test_02(self):
        assert 2+1==3

    def test_03(self):
        assert 1==2

    @allure.tag("tag-me")
    def test_04(self):
        assert True
